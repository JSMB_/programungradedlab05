package polymorphism;

public class Book {
    protected String title;
    private String author;

    public String getTitle() {
        return this.title;
    }

    public String getAuthor() {
        return this.author;
    }

    public Book(String title, String author) {
        this.title = title;
        this.author = author;
    }

    @Override
    public String toString() {
        return "Title: " + this.title + "\nAuthor: " + this.author;
    }
}
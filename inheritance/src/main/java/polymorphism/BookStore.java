package polymorphism;

public class BookStore {
    public static void main(String[] args) {
        Book[] firstList = new Book[5];
        
        //1st and 3rd book
        firstList[0] = new Book("Batman", "Bruce Wayne");
        firstList[2] = new Book("Guilty Crown", "Yōsuke Miyagi");

        //2nd, 4th anmd 5th book
        firstList[1] = new ElectronicBook("Guilty Crown (Kindle)", "Yōsuke Miyagi", 100);
        firstList[3] = new ElectronicBook("Spice And Wolf", "Isuna Hasekura", 250);
        firstList[4] = new ElectronicBook("1984", "George Orwell", 300);

        for(Book list : firstList) {
            System.out.println(list);
        }

        ElectronicBook b = (ElectronicBook) firstList[0];
        System.out.println(b.getNumberBytes());
    }
}

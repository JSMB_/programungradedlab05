package polymorphism;

public class ElectronicBook extends Book {
    private int numberBytes;


    public ElectronicBook(String title, String author, int numberBytes) {
        super(title, author);
        this.numberBytes = numberBytes;
    }

    @Override
    public String toString() {
        return  "Title: " + this.title + "\nAuthor: " + getAuthor() + "\nNumber of Bytes: " + this.numberBytes;
    }

    public int getNumberBytes() {
        return numberBytes;
    }
}

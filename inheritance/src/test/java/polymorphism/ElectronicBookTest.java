package polymorphism;

import org.junit.Assert.*;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ElectronicBookTest {
    @Test
    public void makeAnElectronicBook() {
        ElectronicBook batman = new ElectronicBook("dark knight", "Bruce", 100);
        assertEquals("These two titles should be the same", "dark knight", batman.getTitle());
        assertEquals("These two authors should be the same", "Bruce", batman.getAuthor());
        assertEquals("These two sizes in Bytes should be the same", 100, batman.getNumberBytes(), 0);
    }
}
